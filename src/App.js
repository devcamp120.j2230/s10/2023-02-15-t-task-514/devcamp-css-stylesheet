import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import avatar from "./assets/images/avatar.jpg";

function App() {
  return (
    <div className='devcamp-container container'>
      <div>
        <img src={avatar} alt="avatar" className='devcamp-avatar'></img>
      </div>
      <div>
        <p className='devcamp-quote'>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className='devcamp-user'>
        <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;
